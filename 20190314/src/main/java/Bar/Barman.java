package Bar;

import java.util.Random;

public class Barman implements Runnable {

    public static final String[] MENU = {"mojito", "sex on the beach", "cuba libre"};
    private final Bar bar;

    public Barman(Bar bar) {
        this.bar = bar;
    }

    @Override
    public void run() {
        Random rnd = new Random();
        while (!Thread.interrupted()) {
            try {
                bar.put(MENU[rnd.nextInt(MENU.length)]);
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                System.out.println(ex);
                break;
            }
        }
    }
}
