package Bar;

import java.util.Scanner;

public class BarMain {

    public static void main(String[] args) {

        Bar bar = new Bar();
        Barman barman = new Barman(bar);
        Client cli1 = new Client(bar, "Marek");
        Client cli2 = new Client(bar, "Mirek");
        Client cli3 = new Client(bar, "Maniek");

        Thread th0 = new Thread(barman);
        Thread th1 = new Thread(cli1);
        Thread th2 = new Thread(cli2);
        Thread th3 = new Thread(cli3);

        th0.start();
        th1.start();
        th2.start();
        th3.start();

        Scanner scn = new Scanner(System.in);
        scn.next();

        th0.interrupt();
        th1.interrupt();
        th2.interrupt();
        th3.interrupt();
    }
}
