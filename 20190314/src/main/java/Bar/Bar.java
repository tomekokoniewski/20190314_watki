package Bar;

import java.util.ArrayList;
import java.util.List;

public class Bar {

    private final List<String> drink = new ArrayList<>();

    public synchronized String take() throws InterruptedException {
        while (drink.isEmpty()) {
            wait();
        }
        String drnk = drink.get(0);
        drink.remove(0);
        return drnk;
    }

    public synchronized void put(String drink) {
        this.drink.add(drink);
        notifyAll();
    }
}
