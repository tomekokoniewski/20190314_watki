package Bar;

public class Client implements Runnable {

    private final Bar bar;
    private final String name;

    public Client(Bar bar, String name) {
        this.bar = bar;
        this.name = name;
        
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                String drink = bar.take();
                System.out.println(name + " "+ drink);
                Thread.sleep(drink.length()*1000);
            } catch (InterruptedException ex) {
                System.out.println(ex);
                break;
            }
        }
    }
}
