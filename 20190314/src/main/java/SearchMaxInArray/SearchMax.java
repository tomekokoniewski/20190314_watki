package SearchMaxInArray;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SearchMax implements Runnable {

    private int[] data;
    private int start;
    private int end;
    private int limit;
    private int result;

    public SearchMax(int[] data, int start, int end, int limit) {
        this.data = data;
        this.start = start;
        this.end = end;
        this.limit = limit;

    }

    @Override
    public void run() {
        try {
            result = findMax(data, start, end, limit);
        } catch (InterruptedException ex) {
           System.err.println(ex);
        }
    }

    private int findMax(int[] data, int start, int end, int limit) throws InterruptedException {
        if (end - start > limit) {
            int split = (start + end) / 2;
            
            SearchMax searchMax = new SearchMax(data, start, split, limit);
            Thread thread = new Thread(searchMax);
            thread.start();
            
            int m1 = findMax(data, split + 1, end, limit);
            
            thread.join(); //czekamy aż skończy się wątek
            int m2 = searchMax.getResult();
            return Math.max(m1, m2);
        } else {
            int max = data[start];
            for (int i = start; i < end; i++) {
                if (max < data[i]) {
                    max = data[i];
                }
            }
            return max;
        }
    }

    
    public int getResult() {
        return result;
    }
}
