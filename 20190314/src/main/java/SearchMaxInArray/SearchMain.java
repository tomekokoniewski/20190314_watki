package SearchMaxInArray;

import java.util.Random;

public class SearchMain {

    public static void main(String[] args) throws InterruptedException {
        int[] input = new int[1000];
        Random rnd = new Random();
        for (int i = 0; i < input.length; i++) {
            int j = input[i];
            input[i] = rnd.nextInt();
        }
        SearchMax searchMax = new SearchMax(input,0,input.length-1,200);
        Thread thread = new Thread(searchMax);
        thread.start();
        thread.join();
        System.out.println(searchMax.getResult());
    }
    
}
