package pl.com.tok;


/**
 * @author tomek
 */
public class Sleeper {

    public static void main(String[] args) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000 * 3);
                    System.out.println("Wyspałem się ..");

                } catch (InterruptedException ex) {
                    System.out.println("Pobudka");
                }
            }
        });

        t1.start();
        
        t1.interrupt();
    }

}
