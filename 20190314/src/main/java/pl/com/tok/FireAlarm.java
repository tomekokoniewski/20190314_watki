
package pl.com.tok;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**

 * @author tomek
 */
public class FireAlarm implements Runnable
{

    
    private boolean checkStatus(){
        
        boolean alarm = false;
        
        Random rnd = new Random();
        
        for (int i = 0; i < 5; i++) {
            try {
                if(rnd.nextInt(500)<5){
                    alarm = true;
                }
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(FireAlarm.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }
        
        return !alarm;
    }
    
    
    @Override
    public void run() {
        while(true){
            System.out.println(Thread.currentThread().getName()
            +(checkStatus()?" OK":"Alarm!!"));
        }


    }
    
    
    
    
}