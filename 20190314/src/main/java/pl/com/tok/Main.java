package pl.com.tok;

/**
 * @author tomek
 */

class MyThread implements Runnable {

    private int counter = 0;
    
    
    @Override
    public void run() {
        while (true) {
            counter++;
            if(counter % 10000 ==0 ){
            System.out.println(Thread.currentThread().getName()+": "+counter);
            }
        }
    }
}

public class Main {

    public static void main(String[] args) {

        Thread t1 = new Thread(new MyThread(), "P_min");
        Thread t2 = new Thread(new MyThread(), "P_nor");
        Thread t3 = new Thread(new MyThread(), "P_max");

        t1.setPriority(Thread.MIN_PRIORITY);
        t3.setPriority(Thread.NORM_PRIORITY);
        t3.setPriority(Thread.MAX_PRIORITY);
        
        t1.start();
        t2.start();
        t3.start();
    }
}
