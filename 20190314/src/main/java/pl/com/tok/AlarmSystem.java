package pl.com.tok;

/**
 * @author tomek
 */
public class AlarmSystem {

    public static void main(String[] args) {

        Thread fireAlarmSystem = new Thread(new FireAlarm(), "PPoż");
        Thread antiThiefAlarm = new Thread(new AntiThiefAlarm(), "Przeciwwłamaniowy");
        Thread meteoAlarm = new Thread(new MeteoAlarm(), "Meteo");

        fireAlarmSystem.start();
        antiThiefAlarm.start();
        meteoAlarm.start();

    }

}
