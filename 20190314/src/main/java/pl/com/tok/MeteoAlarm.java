package pl.com.tok;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tomek
 */
public class MeteoAlarm implements Runnable {


    @Override
    public void run() {
        int counter = 0;
        while(true){
            try{
                Thread.sleep(3000);
                System.out.println(Thread.currentThread().getName()+": "
                +((counter % 5 == 0)?"Alarm!!!":"ok"));
                counter++;
            }catch (InterruptedException ex){
                 Logger.getLogger(AntiThiefAlarm.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
}
