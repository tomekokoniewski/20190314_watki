package Counter;

/**
 * @author tomek
 */
public class CounterMain {

    public static void main(String[] args) throws InterruptedException {
        
        Counter c1 = new Counter("C1");
        Counter c2 = new Counter("C2");
        
        Thread t1 = new Thread(c1, "thread_1");
        Thread t2 = new Thread(c2, "thread_2");
        
        Counter c3 = new Counter("C3");
        
        Thread t3 = new Thread(c3, "thread_3");
        Thread t4 = new Thread(c3, "thread_4");
        
        t1.start();
 //       t2.start();
 //       t3.start();
 //       t4.start();
        Thread.sleep(3000);
        
        t1.join();
        
        t2.start();
    }
        
}
