package Counter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tomek
 */
public class Counter implements Runnable {

    private String name;
    private final Object lock = new Object();  //jeżeli static - wszystke wykonają się po kolei
    

    public Counter(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        synchronized (lock) {
            for (int i = 0; i < 10; i++) {
                System.out.println("Wątek: " + Thread.currentThread().getName()
                        + ", obiekt: " + name + ", i:" + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

}
